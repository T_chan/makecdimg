/*
 ===============================================================================
 Discjuggler .cdi format for Dreamcast
 ===============================================================================

 PER SESSION:
 - 2 bytes = number of tracks in this session

	PER TRACK:

 - end of session: 13 bytes (or 12 for V2)


 Session = 
 1) Lead-In
	The overhead costs for the first session are 11250 units (22.5 MB), for each subsequent one - 6750 (13.5 MB)

 2) One or more tracks form a program area (Program Area)
	At the beginning of each track, a pre-gap is formed, containing its parameters, with a size of 150 blocks 
   (300 kb, 2 seconds) for the same type of tracks and 225 blocks (450 kb, 3 seconds) for tracks of different types.
    so: pre-gap for audio track as 1st track, or if previous track was audio track: 150 x 2352 = 352.800 bytes
    so: pre-gap for audio track if previous track was data track: 225 x 2352 = 529.200 bytes (0x81330)
	so: pre-gap for data  track as 1st track, or if previous track was data  track: 150 x 2336 = 350.400 bytes
	so: pre-gap for data  track if previous track was audio track: 225 x 2336 = 525.600 bytes (0x80520)

    The minimum unit of information recorded on a CD-R at one time is a track in CD-DA or CD-ROM format.
    The minimum length of the track is 300 blocks (600 kb, 4 seconds).

 3) Lead-Out
    Lead-out Area of the 1st session: 90 seconds = 6750 sectors
    Lead-out Area of the other sessions: 30 seconds = 2250 sectors

 Hence: 11250 + 150 + 300 (or 302)
  ... but the cdi format does not save the Lead-In, but we need to know that to take that into account for the LBA.

 so cdi format audio/data, with minimal (dummy) audio track:
 Session 1:
    - (no Lead-In Area)
    - 150 sectors (x 2352 bytes) of pre-gap (all zeroes - audio silence)
    - 302 sectors (x 2352 bytes) of audio track 1 (300 sectors possible on certain writers, but best avoided)
	   (of course, more sectors is possible, if you want to put real audio in here)
    - if more than 1 track, per track: 0 gap/pause, then x sectors * 2352 bytes
    - (no Lead-Out Area)
 Session 2:
    - (not in .cdi, but counted for the start LBA: 11250 sectors of Lead-In Area)
    - 150 2336-byte sectors of data pre-gap (cdi4dc fills certain bytes with info)
 		gap between audio & data track:  sector size: 2336   gap_sector_count = 75 for gap1 & 75 for gap2
		? gap1 seems to be mode2 form 2 (8 bytes: 00 00 20 00 x 2, end: 3F 13 B0 BE)
		? gap2 begins with 8 0-bytes, then 13bytes filled in (TDI.P...), ending with 280 bytes filled in
		   -> Mode_2_Form_1 ?
    - the data track, in Mode_2_Form_1. Only 2336 bytes are written in the .cdi, so not the sync + header.
    - post-gap: 150 sectors, written as gap 1
	- (??? not in .cdi: 2250 sectors of Lead-Out Area)

 8 bytes of subheader data seems to be always 0 in cdi file

 cdi format for data/data:
 Session 1:
    - (not in .cdi: 11250 sectors of Lead-In Area)
    - 150 sectors (2336-byte) full of zeroes
    - data (first 16 sectors = all zeroes, no ip.bin here)
    - (??? not in .cdi: 6750 sectors of Lead-Out Area)
 Session 2:
   - (??? not in .cdi: 6750 sectors of Lead-In Area)
   - 150 2336-byte sector of full zeroes
   - data
   - post-gap: 150 sectors, written as gap 1 (or zeroes ?)
   - (??? not in .cdi: 2250 sectors of Lead-Out Area)

 Note: is there a point of data/data ? might be just to cover burners who couldn't write audio/data... 
  the sakura taisen dreamcast does not seem to support this anyways

 In the .cdi image however, the Mode 2 Form 1 tracks are stored with 2336 bytes/sector 
   (2048 userdata bytes + 288 bytes)

 The remainder of the data contains : sync bytes, header bytes, subheader bytes and third layer error correction bytes (EDC and ECC).


 note: raw vs cooked read mode: cooked = 2048 bytes 


*/

