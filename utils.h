/* KallistiOS ##version##

   utils.h
   (c)2021 T_chan

*/
#ifndef __UTILS_H
#define __UTILS_H

#include <stdint.h>

#define TRACK_MODE_CDDA 0
#define TRACK_MODE_MODE_1 1
#define TRACK_MODE_MODE_2 2

typedef struct track_info
{
    char name[2048];        //TODO: copy this over ?
    uint32_t pregap_length; /*number of sectors in the pregap*/
    uint32_t input_size;    /*input file size in bytes*/
    uint32_t mode;          /*TRACK_MODE_CDDA, ...*/
    uint32_t sector_size;   /*in bytes: 2048, 2336, 2352*/
    uint32_t num_sectors;
    uint32_t session_id; /*session nr to which this track belongs to*/
} track_info_t;

typedef struct session_info
{
    uint32_t num_tracks;
    track_info_t tracks[99];
} session_info_t;

typedef struct disc_info
{
    //TODO char* name;//TODO: copy this over ? char name[2048];//TODO: copy this over ?
    uint32_t num_sessions;
    session_info_t sessions[99];
    /*temp variables*/
    uint32_t current_lba; /*a disc begins with a Lead-in Area of 11250 sectors*/
} disc_info_t;
#endif