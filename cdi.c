/* KallistiOS ##version##

   cdi.c
   (c)2021 T_chan

*/
#include <stdio.h>
#include <stdint.h>
#include "utils.h"
#include "cdi.h"

#define WRITEBYTE(x) \
    u8 = x;          \
    fwrite(&u8, 1, 1, fout)
#define WRITEWORD(x) \
    u16 = x;         \
    fwrite(&u16, 2, 1, fout)
#define WRITEDWORD(x) \
    u32 = x;          \
    fwrite(&u32, 4, 1, fout)
#define WRITEBUF(x, y) fwrite(x, y, 1, fout)
#define WRITEZEROES(x, y) WRITEBUF(x, y)

void write_disc_header(FILE *fout, disc_info_t *disc)
{
    //TODO
}

void write_session_end_header(FILE *fout, session_info_t *session)
{
    char buf[12] = {0};
    buf[0] = 0x02;
    WRITEBUF(buf, 12);
}

void write_track_header(FILE *fout, disc_info_t *disc, track_info_t *track)
{
    uint32_t u32;
    uint16_t u16;
    uint8_t u8;
    char buf[32] = {0};

    WRITEDWORD(0);
    WRITEBUF(CDI_TRACK_START_MARK, 10);
    WRITEBUF(CDI_TRACK_START_MARK, 10);
    WRITEDWORD(0); //TODO? V2: zeroes -  (Delicious CDI_V3: 0102014B 0102014B)

    WRITEBYTE((uint8_t)strlen(track->name));
    WRITEBUF(track->name, u8);

    WRITEZEROES(buf, 11); //TODO UNKNOWN
    WRITEDWORD(2);        //TODO UNKNOWN - seems always to be 0x00000002
    WRITEDWORD(0);
    WRITEDWORD(0x000514C8);                          //TODO UNKNOWN - seems always to be 000514C8 (V2 & V3)
    WRITEWORD(2);                                    //TODO UNKNOWN - seems always to be 0x0002 (V2 & V3)
    WRITEDWORD(track->pregap_length);                //usually 0x96
    WRITEDWORD(track->num_sectors);                  //0x3A00  0x012E  0x6108 0x031F 0x025D6A 012E
    WRITEZEROES(buf, 6);                             //TODO UNKNOWN
    WRITEDWORD(track->mode);                         //02 02 00 02 02 02
    WRITEZEROES(buf, 12);                            //TODO ? 2nd track contains a 1 here
    WRITEDWORD(disc->current_lba);                   // 0   0x6688  0 8D90 0 0289F2
    u32 = track->pregap_length + track->num_sectors; //TODO 3A96  01C4  619E 03B5 025E00 01C4
    WRITEDWORD(u32);
    WRITEZEROES(buf, 16);           //TODO UNKNOWN
    WRITEDWORD(track->sector_size); //01 01 02 01 01 01
    WRITEZEROES(buf, 29);           //TODO ? 04 00 00 00 00 96 3A (rest is zeroes)    //04 00 00 00 00 C4 01 (rest is zeroes)
                                    //00 00 00 00 00 9E 61 (rest is zeroes)    //04 00 00 00 00 B5 03 (rest is zeroes)
                                    //04 00 00 00 00 00 5E 02 (rest is zeroes) //04 00 00 00 00 C4 01 (rest is zeroes)
    /*Note: if not V2:
            fseek(fsource, 5, SEEK_CUR);
            fread(&temp_value, 4, 1, fsource);
            if (temp_value == 0xffffffff)
                fseek(fsource, 78, SEEK_CUR); // extra data (DJ 3.00.780 and up)*/
}

void write_header(FILE *fout, disc_info_t *disc)
{
    uint32_t i, j, u32;
    uint16_t u16;
    long header_start;
    session_info_t *session;
    track_info_t *track;

    header_start = ftell(fout);

    WRITEWORD((uint16_t)disc->num_sessions);

    for (i = 0; i < disc->num_sessions; i++)
    {
        session = &disc->sessions[i];
        WRITEWORD((uint16_t)session->num_tracks);
        for (j = 0; j < session->num_tracks; j++)
        {
            track = &session->tracks[j];
            write_track_header(fout, disc, track);
        }
        write_session_end_header(fout, session);
    }

    //TODO?: 0x38 bytes missing still
    write_disc_header(fout, disc);

    WRITEDWORD(CDI_VERSION_2_0);
    WRITEDWORD(header_start);
}

void cdimage_write(FILE *fout, disc_info_t *disc)
{
    uint32_t i, j, num_sectors_written;
    session_info_t *session;
    track_info_t *track;

    disc->current_lba = 11250; /*a disc begins with a Lead-in Area of 11250 sectors*/

    for (i = 0; i < disc->num_sessions; i++)
    {
        session = &disc->sessions[i];
        for (j = 0; j < session->num_tracks; j++)
        {
            track = &session->tracks[j];
            if (track->pregap_length)
            {
                num_sectors_written = write_pregap(fout, track);
                track->pregap_length = num_sectors_written;
                disc->current_lba += num_sectors_written;
            }
            num_sectors_written = write_track(fout, track);
            track->num_sectors = num_sectors_written;
            disc->current_lba += num_sectors_written;
        }
    }

    //TODO pTracks[track_nr].num_sectors = num_sectors;
    //TODO disc->current_lba += num_sectors;

    write_header(fout, disc); /*.cdi header (a footer, in fact)*/

    //write_header
    /* //post-gap is needed after the last data track (preceding the Lead-out)
    write_gap_interval_1 (150, 2336);
    write_footers();*/
}

/*===================================================================================================
TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO 
TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO 
TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO */

/*void write_gap_interval_1(uint32_t num_sectors, uint32_t sector_size)
{
    char buf[2352] = {0};
    int i;

    buf[2] = buf[6] = 0x20;      //the sector mode byte should be that of the current track
    buf[sector_size - 4] = 0xBE; //? Control field of the q-channel ?? TODO
    buf[sector_size - 3] = 0xB0;
    buf[sector_size - 2] = 0x13;
    buf[sector_size - 1] = 0x3F;

    for (i = 0; i < num_sectors; i++)
    {
        fwrite((void *)buf, sector_size, 1, fout);
    }
}*/

/*void write_track(uint32_t track_nr)
{ //, uint32_t is_last_track
    FILE *fin;
    int file_length;

    fin = fopen(pTracks[track_nr].name, "r");
    if (!fin)
    {
        //TODO
        return;
    }
    file_length = fseek(fin, 0, SEEK_END);
    pTracks[track_nr].size = (uint32_t)file_length;
    //TODO detect file type: if 0x014344303031 at 0x8000 -> iso file -> data
    //else: should be a multiple of 2352
    //else: aborting  -> set pTracks[track_nr].mode CDI_SECTOR_MODE_CDDA or CDI_SECTOR_MODE_MODE2
    if (pTracks[track_nr].mode == CDI_SECTOR_MODE_CDDA)
    {
        pTracks[track_nr].sector_size = CDI_SECTOR_USERDATA_SIZETYPE_CDDA;
        pTracks[track_nr].sector_size_in_bytes = 2352;
    }
    else
    {
        pTracks[track_nr].sector_size = CDI_SECTOR_USERDATA_SIZETYPE_MODE2;
        pTracks[track_nr].sector_size_in_bytes = 2336;
    }

    pTracks[track_nr].pregap_length = 150; //0x96

    //audio track: add 2 seconds of silence, data track: add a pre-gap
    write_zeroes(150, pTracks[track_nr].sector_size_in_bytes);
    disc->current_lba += 150;

    fseek(fin, 0, SEEK_SET);
    if (pTracks[track_nr].mode == CDI_SECTOR_MODE_CDDA)
    {
        write_audio_sectors(fin, (uint32_t)file_length, track_nr);
    }
    else
    {
        write_data_sectors(fin, (uint32_t)file_length, track_nr);
    }

    fclose(fin);
}
*/