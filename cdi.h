/*==============================================================================
 Discjuggler .cdi format
 ===============================================================================
 - The file "header" is in fact located at the end of the file, together with 
    session/track info
 - Disc: does not contain Lead-In/Out/TOC info
         does contain pre-gaps
 - Sector: does not contain sync/header info -> not true, if "RAW read" is selected: 2352 bytes are read & put in file
           contains subheader, data, EDC & ECC -> not true for 2048 mode I presume
 
 Structure of a .cdi file:
 - [pre-gap & track data] (1 or more)
 - Sessions Header (1)
 - [Tracks Header] (1 per session)
    - [Track Info]
 - Disc Header (1)
 - Magic Header -> points to the Session Header

 The Magic Header of the file is contained in the last 8 bytes of the file:
 - 4 bytes of version:
    #define CDI_VERSION_2_0 0x80000004 v2.0
    #define CDI_VERSION_3_0 0x80000005 v3.0
    #define CDI_VERSION_3_5 0x80000006 v3.5
 - 4 bytes of header_offset
	if CDI_V35 -> header_offset is relative to the file end (filesize-offset)
	 else header_offset is absolute
 
 The Sessions Header (pointed to by the Magic Header):
 - 2 bytes = number of sessions

 The Tracks Header (1 per session)
 - 2 bytes = n = number of tracks in this session. Followed by n Track Info

 The Track Info (1 per track)
 - 4 bytes of zeroes UNK_1_4
    if not zeroes (DJ 3.00.780 and up) -> 8 extra bytes follow
 - 10 bytes of TRACK_START_MARK
 - 10 bytes of TRACK_START_MARK
 - 4 bytes of UNKNOWN UNK_2_4 - CDI_V3: 0102014B, CD_V35: 7B10A30C CDI_V2: zeroes ? 
 - 1 byte of track name length = n
 - n bytes containing the track name (not terminated by a zero)
 - 11 bytes of UNKNOWN UNK_3_11- zeroes
 - 4 bytes of UNKNOWN UNK_4_4 - always 0x00000002
 - 4 bytes of UNKNOWN UNK_5_4 - always zeroes
 - 4 bytes of UNKNOWN UNK_6_4 - sometimes 0x00057E40, sometimes 000514C8 = disc size in sectors=4 bytes, flag extended struct - then 8 extra bytes()
    if 0x80000000 (DJ4), 8 extra bytes follow: 0x00057E40, then 0x00980000 (0x00980000 = 9.5 M)
	//found by T: 0x000514C8 = 333.000 = number of sectors on a 74 min/650 MB CD
	//found by T: 0x00057E40 = 360.000 = 80 min / 700 MB CD
 - 2 bytes of UNKNOWN UNK_7_2 - usually 0x0002
 - 4 bytes of pregap_length (usually 0x96 or 0x00)
 - 4 bytes of num_sectors 
 - 6 bytes of UNKNOWN UNK_8_6
 - 4 bytes of track mode
 - 4 bytes of UNKNOWN UNK_9_4 - always zeroes ?
 - 4 bytes of session_nr (0 to x)
 - 4 bytes of track_nr (0 to x)
 - 4 bytes of start LBA
 - 4 bytes of total_length (pre-gap + num_sectors)
 - 16 bytes of UNKNOWN UNK_10_16
 - 4 bytes of sector size type
 
 - 29 bytes of UNKNOWN UNK_11_29
 - 1 byte of UNKNOWN UNK_11_A_1  //always 00 for audio, 04 for data ?
 - 4 bytes of UNKNOWN UNK_11_B_4 //always zeroes
 - 4 bytes of total_length2 -> same as the value above total_length (pre-gap + num_sectors) -> why twice ? size on disk maybe ?
 - 20 bytes of UNKNOWN UNK_11_C_20 //always zeroes

 - if version is not V2:
     - 5 bytes of UNKNOWN (always 00 FF FF FF FF)
	 - 4 bytes -> if 0xFFFFFFFF -> 78 (0x4E) bytes of UNKNOWN extra data (DJ 3.00.780 and up)

 - session end: 12 bytes, first is 0x02


 The disc header (V2 header is much smaller)
 - 2 bytes = 0x0000
 - 4 bytes of zeroes UNK_1_4
 - 10 bytes of TRACK_START_MARK
 - 10 bytes of TRACK_START_MARK
 - 4 bytes of UNKNOWN UNK_2_4 - CDI_V3: 0102014B, CD_V35: 7B10A30C
 - 1 byte of track name length = n
 - n bytes containing the track name (not terminated by a zero)
 - 11 bytes of UNKNOWN UNK_3_11- zeroes
 - 4 bytes of UNKNOWN UNK_4_4 - always 0x00000002
 - 4 bytes of UNKNOWN UNK_5_4 - zeroes? sometimes 0x80000000
 - 4 bytes of UNKNOWN UNK_6_4 - sometimes 0x00057E40, sometimes 000514C8
    if 0x80000000 (DJ4), 8 extra bytes follow
 - 4 bytes of number of sectors: snappers: B1 D4 04 00 -> 316593 sectors
 
 
 - 1 byte of disc name length (9 here)
 - n bytes of name (09CHAIRES)
 - 174 remaining bytes
*/
#ifndef __CDI_H
#define __CDI_H

#define CDI_VERSION_2_0 0x80000004 /*v2.0*/
#define CDI_VERSION_3_0 0x80000005 /*v3.0*/
#define CDI_VERSION_3_5 0x80000006 /*v3.5*/

#define CDI_SECTOR_MODE_CDDA 0
#define CDI_SECTOR_MODE_MODE1 1
#define CDI_SECTOR_MODE_MODE2 2

#define CDI_SECTOR_USERDATA_SIZETYPE_MODE1 0 /*2048*/
#define CDI_SECTOR_USERDATA_SIZETYPE_MODE2 1 /*2336*/
#define CDI_SECTOR_USERDATA_SIZETYPE_CDDA 2  /*2352*/

char CDI_TRACK_START_MARK[10] = {0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF};
#endif