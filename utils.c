/* KallistiOS ##version##

   utils.c
   (c)2021 T_chan

*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "utils.h"

void write_zeroes(FILE *fout, uint32_t num_sectors, uint32_t sector_size)
{
    char buf[2352] = {0};
    uint32_t i;

    for (i = 0; i < num_sectors; i++)
    {
        fwrite((void *)buf, sector_size, 1, fout);
    }
}

uint32_t write_pregap(FILE *fout, track_info_t *track)
{
    if (track->mode == TRACK_MODE_CDDA)
    {
        write_zeroes(fout, 150, 2352);
        return (150);
    } //TODO other types

    //write_data_pregap(fout);
    return (150);
}

uint32_t write_dummy_audio_track(FILE *fout)
{
    write_zeroes(fout, 302, 2352);
    return (302);
}

uint32_t write_track(FILE *fout, track_info_t *track)
{
    uint32_t i, rest, num_sectors;
    char buf[2352];

    TODO int add_padding < -set to true if audio
                               TODO write_dummy_audio_track

                                   num_sectors = file_length / sector_size;
    rest = file_length % sector_size;

    for (i = 0; i < num_sectors; i++)
    {
        fread(buf, sector_size, 1, fin);
        fwrite(buf, sector_size, 1, fout);
    }

    if (rest)
    {
        if (add_padding)
        {
            printf("WARNING - track is not a multiple of %d bytes, last sector has been padded !\n", sector_size);
            num_sectors++;
            fread(buf, rest, 1, fin);
            fwrite(buf, rest, 1, fout);
            memset((void *)buf, 0x00, sector_size - rest);
            fwrite(buf, sector_size - rest, 1, fout);
        }
        else
        {
            printf("ERROR - track is not a multiple of %d bytes, last rest bytes have been cut off !\n", sector_size);
        }
    }

    TODO if (num_sectors_written < 302)
    {
        printf("WARNING - audio track smaller than 302 sectors, this might give problems on some burners !\n");
    }

    return (num_sectors);
}

int disc_add_session(disc_info_t *disc)
{
    if (disc->num_sessions < 99)
        disc->num_sessions++;
    else
        return 0;

    return 1;
}

disc_info_t *alloc_disc(void)
{
    return ((disc_info_t *)calloc(1, sizeof(disc_info_t)));
}
