/*
 ===============================================================================
 CD Standards:                       https://en.wikipedia.org/wiki/Rainbow_Books
 ===============================================================================
 - Red Book: Lead-In, TOC, audio tracks, Lead-Out
    1 audio sector = 2352 bytes, in between sectors = 882 bytes of error detection & correction & timing control
    75 sectors/second of audio data, max 99 audio tracks
    standardized as IEC 60908

 - Yellow Book: Lead-In, TOC, track 1, ..., track N, Lead-out   (tracks: Mode 1, Mode 2, audio tracks)
    Mode 1 (data in 9660 format) and/or Mode 2 (sound, images, video) and/or audio
	no sessions, 1 Lead-in, 1 Lead-out
	standardized as ECMA-130 and ISO/IEC 10149 

		CD-ROM XA (eXtended Architecture) – a 1991 extension of CD-ROM

 - Orange Book: Session 1 ... N (Lead-In, TOC, tracks, Lead-Out)
    CD-ROM/XA (Extended Architecture) -> Mode 2 Form 1 & Mode 2 Form 2
	    Note that within a track, Mode 2 Form 1 & 2 can be mixed (certain files in form 1, audio/video files in form 2).
	can write red/yellow/green book data
    partially standardized as ECMA-394 & ECMA-395

 - (Green Book = CD-I)

 Also: https://books.google.be/books?id=i3My1jmItyIC&pg=PA14&lpg=PA14&dq=mode+2+form+1+ecc&source=bl&ots=_aGhe8v6xO&sig=ACfU3U2NNSj3t03G27nJdwqiner2TDNvMA&hl=en&sa=X&ved=2ahUKEwi19dKNovnxAhXtMuwKHfgfCC8Q6AEwCXoECAoQAw#v=onepage&q=mode%202%20form%201%20ecc&f=false
       https://www.gnu.org/software/libcdio/libcdio.html#CD-Formats

 1 sector contains these bytes:
 ----------------------------------------------------------------------------------
 Book          Red     Green     Yellow    Yellow    Orange           Orange
 ----------------------------------------------------------------------------------
 Mode         Audio     CD-I     Mode_1    Mode_2    Mode_2_Form_1    Mode_2_Form_2
 ----------------------------------------------------------------------------------
 Sync                    12        12        12        12             12
 Header                   4         4         4         4             4
 SubHeader                8                             8             8
 Data         2352     2328      2048      2336      2048             2324
 EDC                                4                   4             4
 Intermediate                       8                               
 ECC                              276                 276             
 ----------------------------------------------------------------------------------
 Sum          2352     2352      2352      2352      2352             2352
 ----------------------------------------------------------------------------------

 Sync:
    1  byte = 0x00
    10 bytes = 0xFF
    1  byte = 0x00
 Header:
    3 bytes of Sector Address (Minute (bcd8), Second (0-59, bcd8), Frame (0-74, bcd8))
    1 byte of Sector Mode:
                    if 0, the rest of the (2352-16) bytes are 0x00
                    if 1: 2048 bytes of user data follows, then EDC/ECC/... (includes Mode_1)
                    if 2: 2336 bytes of user data follows (includes CD-I & Mode_2)
 SubHeader:
    2 x 4 bytes (repeated for integrity):
        1 byte of File number - 0 = no interleaving
        1 byte of Channel number
        1 byte of Submode
			{//TODO: rewrite
			uint8 EndOfRecord:1;
			uint8 VideoData:1; // true if the sector contains video data
			uint8 ADCPM:1; // Audio data encoded with ADPCM
			uint8 Data.1; // true if sector contains data
			uint8 TriggerOn:1; // OS dependent
			uint8 Form2:1; // true => form 2, false => form 1
			uint8 RealTime:1; // Sector contains real time data
			uint8 EndOfFile:1; // true if last sector of file
			};
        1 byte of Coding information

 EDC: Error-Detection Code
    32-bit CRC applied on bytes 0 to 2063 (ie, everything before the EDC, from sync to Data)
	TODO: Mode 2 form 1/2: to 2071 / 2347 ?
 Intermediate:
    8 bytes of 0x00
 ECC:
    172 bytes of P-Parity - computed on bytes 12 to 2 075 (yellow book, TODO: orange book)
        (ie, everything after the sync & before the ECC)
    104 bytes of Q-Parity - computed on bytes 12 to 2 247 (yellow book, TODO: orange book)
        (ie, everything after the sync & before the Q-Parity)

 Every Data Track must start with a Pre Gap. It is recommended that every uninterrupted
   written data Track is ended with a Post Gap of minimum 2 seconds.
 5.6.5.1 The Pre Gap
 The use of the Pre Gap is clarified in attachment 13.7.
- When the use of a Pre Gap is prescribed in the Yellow Book or the Green Book, the
definitions according to these books must be used. The second part of this Pre Gap
contains the Track Descriptor Block (see chapter 5.6.5.2).
- When no Pre Gap is prescribed according to the Yellow Book or the Green Book, a Pre
Gap of 2 seconds (150 blocks) must be recorded. This Pre Gap contains the Track
Descriptor Block.

Yellow book:
- Pre-gap:
    Needed for:
        * if the 1st track is a data track -> 150 sectors (encoded as the 2nd interval of a pre-gap)
        * if a data track is preceded by an audio track, or a data track with a different sector mode
    Encoded as Pause. Divided into 2 intervals:
        * 1: at least 75 sectors, encoded as the preceding track (control field of the q-channel).
             if previous track=data, the sector mode byte should be that one of the previous track.
        * 2: at least 150 sectors, and this is structured in sectors.
             Control field of the q-channel & the sector mode byte should be that of the current track.
- Post-gap:
    Needed for:
        * the last data track (preceding the Lead-out)
        * a data track that is followed by an audio track
    structured in sectors, at least 150 sectors
	Control field of the q-channel & the sector mode byte should be that of the current track.

TODO Track Descriptor Identification / The Track Descriptor Unit

 Dreamcast CDs are either: ? TODO: verify - Mode 2 in general could be ok ? or Mode2 Form 1&2
 - 1 or more Audio (2352) tracks + 1 Mode 2 Form 1 (2048) track
 - 1 Mode 2 Form 1 (2048) track  + 1 Mode 2 Form 1 (2048) track

 TODO: iso format
 */