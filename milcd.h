/*==============================================================================
 Dreamcast MIL CD
 https://web.archive.org/web/20010314190533/http://www.sega.co.jp/milcd/home.html
 ===============================================================================
 "Music Interactive Live CD" = 'miru cd' ('miru' = 'to see' in japanese)

 Special kind of CD Extra (CD-XA) that contains:
 - a 1st session with audio tracks that can be played in an audio player
 - a 2nd session with program data that can be launched on a Dreamcast 
    Hence this can contain anything that can be run on a Dreamcast (a movie player, a browser, a game, ...)

 Typical MIL CD layout:
 Session 1:
    - (no Lead-In Area)
    - 150 sectors (x 2352 bytes) of audio pre-gap
	- 1 to x audio tracks (x 2352 bytes), without pre-grap
    - (no Lead-Out Area)
 Session 2:
    - 11250 sectors of Lead-In Area
    - 150 sectors of data pre-gap
        TODO ? gap1/gap2 ?
    - 1 data track  TODO investigate if mixed sector modes on the MIL CD
    - ? TODO post-gap ?
	- ? TODO ? Lead-Out Area ?

 Only 8 official MIL CD were made (although there were discussions with 15 companies):
 - Snappers - Nine Chairs: 11 audio tracks
 - Kita e. White Illumination Pure Song and Pictures: TODO 11 audio tracks ?
 - dps - Heartbreak Diary: 4 audio tracks
 - Checkicco no Miru CD: 4 audio tracks
 - Hang the DJ: TODO 2 audio tracks ?
 - Himitsu Original Sound Track: TODO 14 audio tracks ?
 - D2 Original Sound Track + MILCD: TODO 2 audio tracks ?
 - Space Channel 5: 2 audio tracks

*/