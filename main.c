/* KallistiOS ##version##

   main.c
   (c)2021 T_chan

*/
/*  Inspired from code found in SiZiOUS' cdi4dc, flycast & alex-free's cdirip,
     and own investigations (Yellow/Orange books, looking a lot of .cdi files,
	 ...).

    This program should be enough to generate a useable multi-session cd image file,
     to be used in an emulator or for burning.
    It is also build in a way that makes it easy to add other formats in the
     future.

    Prerequisites:
    - create your audio tracks:
     Note: dummy audio tracks can be created with the commandline:
        dd if=/dev/zero of=audio.raw bs=2352 count=302
		(replace 302 with the desired number of sectors. 
		302 is a recommended minimum.)

    - create your main data track:
     Note: dummy audio tracks can be created with the commandline:
        just with 1 file:
        TODO elf -> Bin -> scramble
        genisoimage -C 0,11702 -o tmp.iso 1ST_READ.BIN
		TODO makeip
        ( cat IP.BIN ; dd if=tmp.iso bs=2048 skip=16 ) > data.iso
        TODO: 1 folder

    Creating the disc image:
        makecdimg audio.raw data.iso output.cdi

   To burn on a real cd, it is recommended to use a method like dcload-ip's
    make-cd:
	- create an audio/data cdi:
	    dd if=/dev/zero of=audio.raw bs=2352 count=302
	    genisoimage -C 0,11702 -o tmp.iso 1ST_READ.BIN
	    ( cat IP.BIN ; dd if=tmp.iso bs=2048 skip=16 ) > data.iso
	    makecdimg audio.raw data.iso output.cdi
    - create a data/data cdi:
TODO rewrite this
   This program has not been tested with a real cd/dvd-writer.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "utils.h"
#include "cdi.h"

#include "utils.c"
#include "cdi.c"
//uint32_t num_tracks = 0;
//uint32_t footer_start = 0;

void usage(const char *progname)
{
    printf("%s - build a DiscJuggler .cdi file - ONLY FOR EMULATOR USAGE !\n", progname);
    printf("Usage: %s <audio.raw> <tmp.iso> <out.cdi>\n\n", progname);
    printf("Arguments:\n"
           "  audio.raw - 1 or more .raw audio files -> Session 1\n"
           "  tmp.iso   - last file must be an .iso file -> Session 2\n"
           "  out.cdi   - the resulting .cdi file\n");
}

int main(int argc, char *argv[])
{
    //uint32_t num_tracks;
    //track_info_t* tracks;
    disc_info_t *disc;
    FILE *fout;

    if (argc < 4)
    {
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    disc = disc_alloc();
    if (!disc)
    {
        printf("Unable to alloc() disc\n");
        exit(EXIT_FAILURE);
    }
    disc_add_session(disc);
    file->detecttype->add to session //session_add_track(session, mode, sectorsize, numsectors, filename)
        disc_add_session(disc);

    fout = fopen(argv[argc - 1], "w");
    if (!fout)
    {
        printf("Unable to create output file %s\n", argv[argc - 1]);
        free(disc);
        exit(EXIT_FAILURE);
    }

    cdimage_write(fout, disc);

    fclose(fout);
    free(disc);

    return 0;
}