#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "cdi.h"

#define DBG_SHOW_UNK_FIELDS 1 /*TODO: move elsewhere ?*/

FILE* fin;
uint32_t file_version;
uint32_t sessions_header_offset;
uint16_t num_sessions, num_tracks;

void read_disc_header() {
    long pos;

    pos = ftell (fin);
	printf ("disc header starts at 0x%08lX\n", pos);

}

char char2ASCII (int isHigh, char ch)
{
 if (isHigh) ch = (ch & 0xF0)>>4;
 else ch = ch & 0x0F;

 if (ch > 0x09) ch = ch - 10 + 0x41;/*A to F*/
 else ch += 0x30; /*0 to 9*/

 return (ch);
}

char* fillBufStr(char* pSrc, int size, char* pDst){
    int i;
	char* pRet = pDst;

    for (i=size; i; i--){
        *(&pDst[0]) = char2ASCII(1, *pSrc);
        *(&pDst[1]) = char2ASCII(0, *pSrc);
        *(&pDst[2]) = 0x20;
		pSrc++;
		pDst += 3;
	}
 *pDst = 0x00;

 return (pRet);
}

int containsAllZeroes (char* pBuf, int size){
    int i;

    for (i=size; i; i--){
        if (*pBuf != 0x00) return 0;
		pBuf++;
	}

 return 1;
}

char* get_sector_mode_str(uint32_t sector_mode){
    switch (sector_mode){
        case CDI_SECTOR_MODE_CDDA:
            return ("Audio");
        case CDI_SECTOR_MODE_MODE1:
            return ("Mode_1");
        case CDI_SECTOR_MODE_MODE2:
            return ("Mode_2");
        default:
            break;
    }
 return ("UNKNOWN!");
}

char* get_sector_size_type_str (uint32_t sector_size_type){
    switch (sector_size_type){
        case CDI_SECTOR_USERDATA_SIZETYPE_MODE1:
            return ("2048");
        case CDI_SECTOR_USERDATA_SIZETYPE_MODE2:
            return ("2336");
        case CDI_SECTOR_USERDATA_SIZETYPE_CDDA:
            return ("2352");
        default:
            break;
    }
 return ("UNKNOWN!");
}

void read_track(uint16_t tracknr) {
    uint32_t unk_1_4, unk_2_4, unk_4_4, unk_5_4, unk_6_4, unk_9_4, unk_11_B_4;
    uint16_t unk_7_2;
	uint8_t unk_11_A_1;
    uint32_t pregap_length, num_sectors, sector_mode, start_LBA, total_length, total_length2,  
            sector_size_type, session_nr, track_nr;
    uint8_t name_length;
    char track_start_mark[10];
    char unk_3_11[11];
    char unk_8_6[6];
    char unk_10_16[16];
    char unk_11_C_20[20];//char unk_11_29[29];
    char extra_1_8[8];
    char extra_2_8[8];
    char extra_3_5[5];
    uint32_t extra_4_4;
    char extra_5_78[78];
    char name[8 * 1024];//probably enough...
	char tmpbuf[1024];

    fread (&unk_1_4, 4, 1, fin);//4 bytes of zeroes UNK_1_4
    if (unk_1_4 != 0x00000000){    //if not zeroes (DJ 3.00.780 and up) -> 8 extra bytes follow //TODO: verify if correct - seems always to be ZERO
        fread (extra_1_8, 8, 1, fin);
    }
    fread (track_start_mark, 10, 1, fin); //10 bytes of TRACK_START_MARK
    fread (track_start_mark, 10, 1, fin); //10 bytes of TRACK_START_MARK
    /*TODO: check both track_start_mark*/
    fread (&unk_2_4, 4, 1, fin); //4 bytes of UNKNOWN UNK_2_4 - CDI_V3: 0102014B, CD_V35: 7B10A30C, V2: zeroes ?
    fread (&name_length, 1, 1, fin);//1 byte of track name length = n
    //TODO if name_length bigger than 8K
    fread (name, name_length, 1, fin);//n bytes containing the track name (not terminated by a zero)
    fread (unk_3_11, 11, 1, fin); //11 bytes of UNKNOWN UNK_3_11- zeroes
    fread (&unk_4_4, 4, 1, fin); //4 bytes of UNKNOWN UNK_4_4 - always 0x00000002
    fread (&unk_5_4, 4, 1, fin); //4 bytes of UNKNOWN UNK_5_4 - always zeroes
    fread (&unk_6_4, 4, 1, fin); //4 bytes of UNKNOWN UNK_6_4 - sometimes 0x00057E40, sometimes 000514C8
	//found by T: 0x000514C8 = 333.000 = number of sectors on a 74 min/650 MB CD
	//found by T: 0x00057E40 = 360.000 = 80 min / 700 MB CD
    if (unk_6_4 == 0x80000000){    //if 0x80000000 (DJ4), 8 extra bytes follow
        fread (extra_2_8, 8, 1, fin);
    }
    fread (&unk_7_2, 2, 1, fin); //2 bytes of UNKNOWN UNK_7_2 - always 0x0002
    fread (&pregap_length, 4, 1, fin); //4 bytes of pregap_length (usually 0x96)
    fread (&num_sectors, 4, 1, fin); // 4 bytes of num_sectors 
    fread (unk_8_6, 6, 1, fin); //6 bytes of UNKNOWN UNK_8_6
    fread (&sector_mode, 4, 1, fin); //4 bytes of sector_mode
    fread (&unk_9_4, 4, 1, fin); //4 bytes of UNKNOWN UNK_9_4
    fread (&session_nr, 4, 1, fin); //4 bytes of session_nr (0 to x)
    fread (&track_nr, 4, 1, fin); //4 bytes of track_nr (0 to x)
    fread (&start_LBA, 4, 1, fin); //4 bytes of start_LBA
    fread (&total_length, 4, 1, fin);//4 bytes of total length (pre-gap + num_sectors)
    fread (unk_10_16, 16, 1, fin); //16 bytes of UNKNOWN UNK_10_16
    fread (&sector_size_type, 4, 1, fin); //4 bytes of sector_size_type
    //fread (unk_11_29, 29, 1, fin); //29 bytes of UNKNOWN UNK_11_29
    fread (&unk_11_A_1, 1, 1, fin); //always 00 for audio, 04 for data ?
    fread (&unk_11_B_4, 4, 1, fin); //4 bytes of UNKNOWN UNK_11_B_4 //always zeroes
    fread (&total_length2, 4, 1, fin); //4 bytes of total_length -> same as the value above total_length (pre-gap + num_sectors) -> why twice ? size on disk maybe ?
    fread (unk_11_C_20, 20, 1, fin); //always zeroes

    if (file_version != CDI_VERSION_2_0){
        fread (extra_3_5, 5, 1, fin);//5 bytes of UNKNOWN (? 00 FF FF FF FF)
		fread (&extra_4_4, 4, 1, fin);//4 bytes of UNKNOWN 
		if (extra_4_4 == 0xFFFFFFFF){//if 0xFFFFFFFF -> 78 (0x4E) bytes of UNKNOWN extra data (DJ 3.00.780 and up)*/
            fread (extra_5_78, 78, 1, fin);
        }
    }

    printf("    track %02d (%s-%s): pregap %06d, start_LBA %06d, num_sectors %06d, total %06d, session %01d, track %02d\n",
        tracknr, get_sector_mode_str(sector_mode), get_sector_size_type_str(sector_size_type),
        pregap_length, start_LBA, num_sectors, total_length, session_nr, track_nr);
    printf("    track name: %s\n", name);
#ifdef DBG_SHOW_UNK_FIELDS
    if (unk_1_4 != 0x00000000)
        printf("    unk_1_4: 0x%08X (usually: 0x00000000 - %s\n", unk_1_4,  "UNEXPECTED !");
    if (unk_2_4 != 0x00000000)
        printf("    unk_2_4: 0x%08X (different for all .cdi images ?\n", unk_2_4);
    if (unk_4_4 != 0x00000002)
        printf("    unk_4_4: 0x%08X (usually: 0x00000002 - %s\n", unk_4_4, "UNEXPECTED !");
    if (unk_5_4 != 0x00000000)
        printf("    unk_5_4: 0x%08X (usually: 0x00000000 - %s\n", unk_5_4, "UNEXPECTED !");
    //TODO : cd size uint32_t unk_6_4; char extra_2_8[8];
    if (unk_7_2 != 0x0002)
        printf("    unk_7_2: 0x%04X (usually: 0x0002 - %s\n", unk_7_2, "UNEXPECTED !");
    if (!containsAllZeroes(unk_3_11, 11))  printf("    unk_3_11 not all zeroes!: %s\n", fillBufStr(unk_3_11, 11, tmpbuf));
    if (!containsAllZeroes(unk_8_6, 6))    printf("    unk_8_6 not all zeroes!: %s\n", fillBufStr(unk_8_6, 6, tmpbuf));
    if (unk_9_4 != 0x00000000)
        printf("    unk_9_4: 0x%08X (usually: 0x00000000 - %s\n", unk_9_4, "UNEXPECTED !");
	if (!containsAllZeroes(unk_10_16, 16)) printf("    unk_10_16 not all zeroes!: %s\n", fillBufStr(unk_10_16, 16, tmpbuf));
    if (((sector_mode == CDI_SECTOR_MODE_CDDA) && (unk_11_A_1 !=  0x00)) ||
	    ((sector_mode != CDI_SECTOR_MODE_CDDA) && (unk_11_A_1 !=  0x04)))
        printf("    unk_11_A_1: 0x%02X (usually: 0x00 for Audio, 0x04 for data - %s\n", unk_11_A_1,  "UNEXPECTED !");
    if (unk_11_B_4 != 0x00000000)
        printf("    unk_11_B_4: 0x%08X (usually: 0x00000000 - %s\n", unk_11_B_4,  "UNEXPECTED !");
    if (total_length2 != total_length)
        printf("    total_length2: 0x%08X (usually same as total_length: 0x%08X - %s\n", total_length2, total_length, "UNEXPECTED !");
    if (!containsAllZeroes(unk_11_C_20, 20))    printf("    unk_11_C_20 not all zeroes!: %s\n", fillBufStr(unk_11_C_20, 20, tmpbuf));

    //char extra_1_8[8]; -> alredy covered if unk_1_4 is always 0, this shouldn't even exist
	//TODO 3 5 00 FF FF FF FF
    if (!containsAllZeroes(extra_3_5, 5)) printf("    extra_3_5 not all zeroes!: %s\n", fillBufStr(extra_3_5, 5, tmpbuf));
    if (extra_4_4 != 0x00000000)
        printf("    extra_4_4: 0x%08X (usually: 0x00000000 - %s\n", extra_4_4, "UNEXPECTED !");
    if (!containsAllZeroes(extra_5_78, 78)) printf("    extra_5_78 not all zeroes!: %s\n", fillBufStr(extra_5_78, 78, tmpbuf));

#endif
}

void read_tracks_header() {
    long pos;

    pos = ftell(fin);
	printf("Tracks header offset: 0x%08lX", pos);
    fread (&num_tracks, 2, 1, fin);
	//printf("number of tracks in this session: %d\n", num_tracks);
}

void read_sessions_header() {
	fseek (fin, sessions_header_offset, SEEK_SET);
	fread (&num_sessions, 2, 1, fin);
	printf("number of sessions: %d\n", num_sessions);
}

char* get_version_str(uint32_t version){
    switch (version){
        case CDI_VERSION_2_0:
            return ("v2.0");
        case CDI_VERSION_3_0:
            return ("v3.0");
        case CDI_VERSION_3_5:
            return ("v3.5");
        default:
            break;
    }
 return ("UNKNOWN!");
}

void read_magic_header() {
    long file_length;

    fseek (fin, 0, SEEK_END);
	file_length = ftell(fin);
    //TODO: if file_length < 8 (or that + 1 track) -> invalid file length
    fseek (fin, -8, SEEK_END);
	fread (&file_version, 4, 1, fin);
	fread (&sessions_header_offset, 4, 1, fin);
	if (file_version == CDI_VERSION_3_5)//CDI_V35
        sessions_header_offset = file_length - sessions_header_offset;

    printf("version: %s, sessions header offset: 0x%08X, filesize: 0x%08lX (%ld)\n", 
        get_version_str(file_version),
        sessions_header_offset, file_length, file_length);
}

void usage(const char *progname) {
    printf("%s - displays all the info about a DiscJuggler .cdi file\n", progname);
    printf("Usage: %s <image.cdi>\n\n", progname);
}

int main(int argc, char *argv[]) {
    uint16_t i,j;

    if(argc < 2) {
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    fin = fopen(argv[argc-1], "r");
    if (!fin) {
        printf("Unable to open file %s\n", argv[argc-1]);
        exit(EXIT_FAILURE);
    }

    read_magic_header();
    read_sessions_header();
	for (i=0; i < num_sessions; i++){
        read_tracks_header();
        printf("- session %d: %d tracks:\n", i+1, num_tracks);
        for (j=0; j < num_tracks; j++){	
			read_track(j+1);
        }
		fseek (fin, file_version == CDI_VERSION_2_0 ? 12 : 13, SEEK_CUR);//?? TODO: investigate why
    }
    read_disc_header();
    //TODO: warn if remaining bytes (should be 8 bytes until end of file)

    fclose (fin);
}
